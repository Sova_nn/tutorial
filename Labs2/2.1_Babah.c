#include <stdio.h>
#include <Windows.h>
#define G 9.81

int main()
{
	float h, l;
	int t;
	printf ("Input height\n");
	scanf ("%f", &h);
		
	for (t=0; h>(G*t*t)/2 ; ++t)
	{
		l = (G*t*t)/2;
		h-=l;
		printf ("t=%02dc, h=%.2fm\a", t, h);
		printf ("\r");
		
		Sleep (1000);
	}
	printf ("BABAH\!\n");
	return 0;
}
