#include <stdio.h>

int main()
{
	float temp, value;
	char sign, answer;
	do
	{
	  printf ("Input your value\n");
	  scanf ("%f%c", &value, &sign);
	  if (sign=='R')
	   {
		temp=0.017*value;
	    printf ("In %.2f radians there are %.2f grades\n", value, temp);
	   }
	  else if (sign=='D')
	   {
	    temp=57.3*value;
	    printf ("In %.2f grades there are %.2f radians\n", value, temp);
	   }
	  else 
		printf ("You inputed a incorrect value\n");
	  printf ("If you want to continue, enter Y\n");
	  getchar();
	  answer = getchar();
	}
    while (answer=='Y');
	return 0;
}