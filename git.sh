if (( $# > 0 ))
then
    git add *
    git commit -m "$1"
    git push origin master
else
    echo "Use: $0 commit"
fi