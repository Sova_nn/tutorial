#include<stdio.h>
#include<conio.h>

#define MAXTITLE 150
#define MAXAGE 5
#define STRING 20


/*
struct family
{
	char relatives[MAXTITLE];
	int age[MAXAGE];
}*/

int main (void)
{
	char family[STRING][MAXTITLE];
	char *old, *young;
	int age, agemin=0, count, i=0;
	puts("How much relatives do you have?");
	scanf("%d", &count);

	while (i<count)
	{
		puts("Please, enter your relative name");
		scanf("%s", &family[i]);
		puts("Please, enter his/her age");
		scanf("%d", &age);
		if (age>agemin)
		{
			old=family[i];
			agemin=age;
		}
		else
		{
			young=family[i];
		}
		i++;
	}
	puts("Oldest relative in your family is:");
	puts(old);

	puts("Youngest relative in your family is:");
	puts(young);

	return 0;
}