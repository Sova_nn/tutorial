#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <locale.h>

#define N 100
#define M 150

void sortlen(char **s, int count);

int main(int argc, char *argv[])
{
	char in;
	char buf[N][M];
	char *mass[N];
	int i=0,j=0;

	FILE *file;

	setlocale(LC_ALL,"rus");

	file = fopen("example.txt", "a+");

	while (fgets(buf[i], M-1, file) && i<=N) // ���� �� ��������� �������� Enter
	{
		if(buf[i][strlen(buf[i])-1]=='\n')
			buf[i][strlen(buf[i])-1]='\0';
		mass[i]=buf[i];
		i++;
	}
	
	sortlen(mass,i);
	
	puts ("Here you are\: ");
		
	for (j=0; j<i; j++)
	{
		puts(mass[j]);
		fputs("\n", file);
		fputs(mass[j], file);
	}

	fputs("\n", file);
	fclose(file);
	return 0;
}	

void sortlen(char **s, int count) 
{ // **s == *s[] - ������ ����������
	int i, j;
	char *str;
 
	for (i=0; i<count-1; i++)
	{
		for (j=0; j < count-i-1; j++) 
		{
			if (strlen(s[j]) < strlen(s[j+1])) 
			{
				str = s[j];
				s[j] = s[j+1];
				s[j+1] = str;
			}	
		}
	}
}		