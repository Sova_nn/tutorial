#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define N 6

int main()
{
	int mass[N];
	int i=0,min,max;
	srand(time(NULL));

	for (i=0; i<N; i++)
	{
		mass[i]=(rand()%100);
		printf ("%5d", mass[i]);
	}
	
	max=min=mass[0];
	
	for (i=0; i<N; i++)
	{
		if (mass[i]>max)
		max=mass[i];
	}

	for (i=0; i<N; i++)
	{
		if (mass[i]<min)
		min=mass[i];
	}

	printf ("\n max =%3d \n min =%3d", max, min);

	    
	putchar('\n');

	return 0;
}