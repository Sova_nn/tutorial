#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

#define N 50


int main()
{
	char str[N], *p=str, *s;
	int i=0, j, len=0, sum=0, total=0;
	double r=10;
	puts(" Please enter the numbers\.\n Please, pay attention on range of your numbers\.\n It must be not bigger than 5!");
	fgets(str,50,stdin);
	str[strlen(str)-1]=' ';

	while (*p)
	{
		if (*p!=' ' && i==0)
		{
			i=1;
			len++;
		}
		else if (*p!=' ' && i==1 && len!=5)
		{
			len++;
		}
		
		else if ((*p==' ' && i==1)|| (len==5 && i==1))
		{
			s=p-1;

			for (j=len-1;j>=0;j--)
			{
				sum+= pow(r,j)*(*(s-j)-'0');
			}
				
			i=0;
			len=0;
		}

		
		p++;
	}
	printf ("%d\n", sum);
	return 0;

}