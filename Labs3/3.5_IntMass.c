#include <stdio.h>
#include <time.h>
#include <stdlib.h>


#define N 10

int main()
{
	int mass[N], *s=mass, *p=mass+N;
	int i=0, *min, *max, sum=0, z=0, x=0;
	srand(time(NULL));

	for (i=0; i<N; i++)
	{
		mass[i]=(rand()%100-50);
		printf ("%5d", mass[i]);
	}
	
	while (*s && *p)
	{
		if (*s<0 && z==0)
		{
			min=s;
			z=1;
		}

		if (*p>0 && x==0)
		{
			max=p;
			x=1;
		}

		s++;
		p--;
	}

	printf ("\n min =%4d\, max =%4d", *min, *max);

	while (min!=(max+1))
		{
			sum+= *min;
			min++;
		}
		
	
	printf ("\n summ of elements from min to max =%5d\n", sum);	
    
	putchar('\n');

	return 0;
}