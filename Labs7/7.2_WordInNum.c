/*�������� ���������, ������� ��������� ������ ���� �1,3,6�10,12�
� ��������� ������, ���������� ��� �����, �������� � ���������,
�� ���� �1,3,6,7,8,9,10,12�*/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>

#define N 256

int main ()
{
	char num[N]; // ��������� ��� �������, char num - ��� ������ ������, � int new_num - ��� ������� ���������������� ������
	int new_num[N];
	char *p=num, *s1, *s2; // ��������� 3 ���� ����������: ��� ������� ������ � 2 ��������� ��� ����� ����� �� � ����� ���� ��� ���������� ���������
	int i=1, j; // ��������� ��� ������� ��� ��������� �� ���������� ������ ������� � ��� ������ �����������
	
	puts("Input your string");
	fgets(num, N, stdin);
	num[strlen(num)-1]=0;
	
	new_num[0]=atoi(p);

	while (*p) 
	{
		if(*p==' ' || *p==',')
		{
			s1=(p+1);
			new_num[i]=atoi(s1);
			i++;
		}
		if(*p=='-')
		{
			s2=(p+1);
			while(new_num[i-1]!=atoi(s2))
			{
				new_num[i]=new_num[i-1]+1;
				i++;
			}
		}
		p++;
	}

	for(j=0;j<i-1;j++)
	{
		printf("%d,", new_num[j]);
	}
	printf("%d\n", new_num[i-1]);
	return 0;
}

